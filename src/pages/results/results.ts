import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {

  query1 = this.navParams.get("q");
  query = this.query1;
  query2 = this.query.replace(/\s+/g, " ");
  url = this.query2.replace(/\s/g, "+");

  check_temp : Array<any>;
  check_temp2 : any;

  apiUrl = 'URL'+this.url;
  res: Observable<any>;
  arr : Array<any>;

  constructor(public navCtrl: NavController, private navParams: NavParams, public loadingCtrl: LoadingController, public httpClient: HttpClient, public toastCtrl: ToastController) {
    
    
    this.presentLoading();

    this.res = this.httpClient.get(this.apiUrl);
    this.res
    .subscribe(data => {
    this.arr = data;
    });

  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Searching...",
      duration: 800
    });
    loader.present();
  }


  search(str : any){
    
    if (str != ""){
      str = str.trim();
      this.presentLoading();
      this.navCtrl.push(ResultsPage, {
        "q": str
      });
    }
    else
    {
      this.presentToast();
    }
  
  }

  presentToast() {
    const toast = this.toastCtrl.create({
      message: 'Please enter a search query.',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  linkChecker(scan : any){
     this.check_temp = scan.split("://");
     this.check_temp2 = this.check_temp[0];
     if(this.check_temp2 == 'https'){
        return 1;
     }
     else
     {
       return 0;
     }
  }

}
