import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { ResultsPage } from "../results/results";
///import { HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  query : any = "";
  link : any; 
  //res: Observable<any>;
  //redLink :any;

  str1 : any; str2: any; str3:any;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {

  }

  search(str :any){
    if (str != ""){
      str = str.trim();
      this.navCtrl.push(ResultsPage,{
        "q": str
      });
    }
    else
    {
      this.presentToast();
    }
  }


  lucky(str:any) {
    if (str != ""){
      this.str3 = str.trim();
      this.str1 = str.replace(/\s+/g, " ");
      this.str2 = str.replace(/\s/g, "_");

      this.link = 'http://subhajit.epizy.com/request_handler.php?id='+this.str2;

    window.open(this.link,'_system');
  
    }
    else
    {
      this.presentToast();
    }
    
  }

  presentToast() {
    const toast = this.toastCtrl.create({
      message: 'Please enter a search query.',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
